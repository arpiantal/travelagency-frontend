export class RoomCreateDTO {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
